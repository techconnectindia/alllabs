# Git Lab

* [Clone](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html) `this` repository to your local machine
* On your local machine, create a new [branch](https://confluence.atlassian.com/bitbucket/branching-a-repository-223217999.html), **please follow the instructions for creating a local branch!**
	* Where you see `<feature_branch>` replace that with your first name plus the first letter of your last name. So if your name is Thaneshwara you would create a new branch by using this command: `git branch thaneshwaraM`
* edit the `my-name` file, by adding your name to the file.
* `git add .` your changes to the list of your `commited` files.
* `git commit -m "added your-name-here"` your added files to the list of your `staged` files.
* `git push origin <name_of_your_branch>` your staged files to your remote repository.
	* Where `<name_of_your_branch>` if you are Thaneshwara would be `thaneshwaraM`. 
* submit a `pull request` which compares `master` to your branch `<name_of_your_branch>`.



