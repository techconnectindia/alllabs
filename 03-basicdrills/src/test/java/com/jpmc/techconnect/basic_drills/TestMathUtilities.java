package com.jpmc.techconnect.basic_drills;

import com.jpmc.techconnect.basic_drills.mathutilities.TestAddition;
import com.jpmc.techconnect.basic_drills.mathutilities.TestMultiplication;
import com.jpmc.techconnect.basic_drills.mathutilities.TestSubtraction;
import com.jpmc.techconnect.basic_drills.mathutilities.TestDivision;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestAddition.class,
        TestSubtraction.class,
        TestDivision.class,
        TestMultiplication.class
})
public class TestMathUtilities {
}